package com.mycompany.app;

import org.apache.commons.net.telnet.TelnetClient;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        try {
            Connection telnet = new Connection();
            telnet.connect(args);
            telnet.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class Connection {

        private TelnetClient telnet;
        private InputStream in;
        private PrintStream out;

        public Connection() {

            this.telnet = new TelnetClient();
        }

        public void connect(String[] args) {

            //Scanner scanner = new Scanner(System.in);
            //System.out.println("Enter Host: ");
            //String host = scanner.nextLine();
            //System.out.println("Enter Port: ");
            //String port = scanner.nextLine();
            //System.out.println("Enter Command: ");
            //String command = scanner.nextLine();

            // java -jar maven-telnet.jar ipaddress port command1 command2
            String host = args[0];
            String port = args[1];
            String command = args[2] +" "+ args[3];

            try {
                // Connect to the specified server
                telnet.connect(host, Integer.parseInt(port));

                // Get input and output stream references
                in = telnet.getInputStream();
                out = new PrintStream(telnet.getOutputStream());

                sendCommand(command);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public String readUntil(String pattern) {
            try {
                char lastChar = pattern.charAt(pattern.length() - 1);
                StringBuffer sb = new StringBuffer();
                boolean found = false;
                char ch = (char) in.read();
                while (true) {
                    System.out.print(ch);
                    sb.append(ch);
                    if (ch == lastChar) {
                        if (sb.toString().endsWith(pattern)) {
                            return sb.toString();
                        }
                    }
                    ch = (char) in.read();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        public void write(String value) {
            try {
                out.println(value);
                out.flush();
                System.out.println(value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public String sendCommand(String command) {
            try {
                write(command);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        public void disconnect() {
            try {
                telnet.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
